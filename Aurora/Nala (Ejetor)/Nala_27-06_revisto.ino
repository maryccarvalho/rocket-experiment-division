#include <Wire.h>

const int buttonPin = 2;     // the number of the pushbutton pin
const int gatePin =  12;      // the number of the LED pin
const int ConnectIgn = 9;

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status
int starttime = 10;
int fire = 0;
int val = 0; //valor auxiliar que guarda o valor de input do ConnectIgn


void setup() {
  // initialize the LED pin as an output:
  Serial.begin(9600);
  pinMode(ConnectIgn, INPUT);
  pinMode(gatePin, OUTPUT);
  analogWrite(gatePin, 0);
  digitalWrite(ConnectIgn, LOW);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
  Serial.println("Acordei NALA");
}

void loop() {

  val = digitalRead(ConnectIgn);
  if (val == HIGH) {
    fire = 1;
  } else {
    fire = 0;
  }

  if (fire == 1)
  {
    Serial.println("FIRE!!");
    analogWrite(gatePin, 255);
    while (1);
  }
}
