
#include <SoftwareSerial.h>

//GPS
TinyGPSPlus gps;
SoftwareSerial loraSerial(3, 2); // TX, RX
SoftwareSerial gpsSerial(4,5); // TX, RX

//VARS para ejecao
unsigned long startMillis; 
unsigned long currentMillis;
const unsigned long period = 24000; 
unsigned long starttime=0;
int i;
int wait;
float module = 0;
int flagtime=3;
int flagacel=4;

//Acelerometro
const int MPU_addr=0x68;  // I2C addregpsSerial of the MPU-6050
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;
int accelCounter = 0;

//Cartao SD
const int chipSelect = 10;
File fich;

//LEDS
const int SD_LED = 7; //RED
const int PRESS_LED = 6; //Verde
//Aux da LORA
const int LORA_AUX = 8; 
//Pin Comunicação
const int sendPin = 9;  

//Ejeção
int countneg = 0;
int ejecao = 0;
float mean_acc = 0.0;

/*Inicializa o acelerometro
*Argumentos:Inexistentes
*Return: Inexistente 
*/
void inicializarMPU6050()
{
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  byte resetBin = B10000000; //Usar para dar reset no acell
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission();
  Wire.beginTransmission(MPU_addr); //I2C address of the MPU
  Wire.write(0x1B); //Accessing the register 1B - Gyroscope Configuration (Sec. 4.4) 
  Wire.write(0x00000000); //Setting the gyro to full scale +/- 250deg./s 
  Wire.endTransmission(true); 
  Wire.beginTransmission(MPU_addr); //I2C address of the MPU
  Wire.write(0x1C); //Accessing the register 1C - Acccelerometer Configuration (Sec. 4.5) 
  Wire.write(0b00011000); //Setting the accel to +/- 2g
  Wire.endTransmission(true);
}

/*Retira os valores das acelereções do sensor
* Argumentos: Valores das acelereções nos diferentes eixos
* Return: Inexistente
*/
void getAcel (float *AcXf, float *AcYf, float *AcZf, float *GyXf, float *GyYf, float *GyZf )
{
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
  
  AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)    
  AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

  //Conversao
  //Aceleracao
  *AcXf = AcX/2048.0;
  *AcYf = AcY/2048.0;
  *AcZf = AcZ/2048.0;

  *GyXf = GyX/2048.00;
  *GyYf = GyY/2048.00;
  *GyZf = GyZ/2048.00;
    
  //Termina a funçao
  return;
}


/*Inicializa o cartao SD
* Argumentos: Nao tem
* Return: -1 se falhou
*         1 se iniciou com sucesso
*/
int inicializarSD ()
{
  //Inicialização do cartao SD
  //Ligar o cs no 10
  pinMode (chipSelect, OUTPUT);

  //Se falha a inicializar
  if(!SD.begin(10))
  {
    return -1;
  }
  
  //SucegpsSerialo
  fich = SD.open ("sensores.txt", FILE_WRITE);
  fich.println("Setup SD ");  
  fich.close();
  return 1;
}

/* Guarda os valores no cartao SD
* Argumentos: Valores a guardar no SD
*             float AcXf: Acel X, float AcYf: Acel Y, float AcZf: Acel Z 
*             float tempBMP: temperatura, float pregpsSerialBMP: pregpsSerialao, float altBMP: altitude
* Return: SucegpsSerialo/InsucegpsSerialo
*/
int guardaSD (float AcXf, float AcYf, float AcZf, float GyXf, float GyYf, float GyZf, double latitude, double longi, double altGPS)
{
  //Abre o ficheiro de escrita no SD
  fich = SD.open ("sensores.txt", FILE_WRITE);
     
  //Escreve os dados no SD
  if (fich)
  {
    //Escreve no ficheiro
    //Aceleracoes
    fich.print(AcXf);
    fich.print(" ");
    fich.print(AcYf);
    fich.print(" ");
    fich.print(AcZf);
    fich.print(" ");
    //Gyro
    fich.print(GyXf);
    fich.print(" ");
    fich.print(GyYf);
    fich.print(" ");
    fich.print(GyZf);
    fich.print(" ");
    //GPS
    fich.print(latitude);
    fich.print(" ");
    fich.print(longi);
    fich.print(" ");  
    fich.print(altGPS);
    fich.println();

    //Fecha o ficheiro
    fich.close();
    return 1;
   }
   //Se o ficheiro nao abrir
   else 
   {
     return -1;    
   }

}

/*Valores de altitude longitude e latitude */
void gpsReadVals (double *lati, double *longi, double *altitudeGps)
{
  // get the byte data from the GPS
  gpsSerial.listen();
  while(wait==0){
      while (gpsSerial.available() > 0)
      {
        // get the byte data from the GPS        
        gps.encode(gpsSerial.read());
        if (gps.location.isUpdated())
        {
          digitalWrite(PRESS_LED, HIGH);
          // Latitude in degrees (double)
          *lati = gps.location.lat();     
          // Longitude in degrees (double)
          *longi = gps.location.lng();
          //Altitude
          *altitudeGps = gps.altitude.meters();
          return;
        } else {
          //Acende o LED
          digitalWrite(PRESS_LED, LOW);
        }
      }
    }
}

/*
*Calcula o modulo da aceleração 
*return: 1 se modulo maior que 3
*        0 se modulo inferior a 3
*/
int accelModule(float AcXf, float AcYf, float AcZf)
{
  if (sqrt(pow(AcXf,2)+pow(AcYf,2)+pow(AcZf,2)) > 3)
  {
    return 1;
  }
  else
  {
   return 0;
  }
}

void setup() 
{
  //Inicia os Serial
  //Serial.begin(9600);
  gpsSerial.begin(9600);
  loraSerial.begin(9600);
  int cartaoSD = 0;
  
  //Pin do Aux
  pinMode(LORA_AUX, INPUT);

  //Pin do slave
  pinMode(sendPin, OUTPUT);
  digitalWrite(sendPin, LOW);
  
  //Inicializa leds
  pinMode(SD_LED, OUTPUT);
  pinMode(PRESS_LED, OUTPUT);
  //Coloca os todos desligados
  digitalWrite(SD_LED, LOW); 
  digitalWrite(PRESS_LED, LOW);  
  
  //Inicializa a biblio Wire
  Wire.begin();
  
  //Inicializa o Acel
  inicializarMPU6050();

  //Inicializa SD
  cartaoSD = inicializarSD();
  //Se for correctamente iniciado acende o led
  if (cartaoSD == 1)
  {
    //Acende o LED
    digitalWrite(SD_LED, HIGH);   
  }
}


void loop() 
{
  //Valores Acell
  float AcXf = 0.00;
  float AcYf = 0.00;
  float AcZf = 0.00;
  float modAc = 0.00;
  //Valores Girosc
  float GyXf = 0.00;
  float GyYf = 0.00;
  float GyZf = 0.00;
  //GPS
  double lati = 0.0000000;
  double longi = 0.0000000;
  double altitudeGps = 0.0000000;
  

 
  //Valores do GPS
  wait=1;
  gpsReadVals(&lati, &longi, &altitudeGps);
   
  //Imprimir no Serial
  //Envia a string pelo Lora
  String s = String(lati,5) + " " + String(longi,5) + " " + String(altitudeGps);
  
  loraSerial.listen();
  //Verifica o valor do aux
  if(digitalRead(8))
  {
    loraSerial.println(s);
    delay(500);
  }
  
  String input = loraSerial.readString();

  if(input.indexOf("RED_SOS") != -1){
    ejecao=1; 
    loraSerial.println("A AURORA RECEBEU A EMERGENCIA");  
  }

  if(input.indexOf("RED_TIMER") != -1){
      flagtime=1;
      startMillis = millis();  //initial start time
      loraSerial.println("A AURORA RECEBEU O TIMER");
  }

  if(input.indexOf("RED_ACCEL") != -1){
      flagacel=1;
      loraSerial.println("A AURORA RECEBEU A ACELERAÇÃO");
  }

  //Obtem aceleracao
  getAcel (&AcXf, &AcYf, &AcZf, &GyXf, &GyYf, &GyZf); 
  //Guarda os valores no cartao SD
  guardaSD (AcXf, AcYf, AcZf, GyXf, GyYf, GyZf, lati, longi, altitudeGps);

  //EJECAO
  //Mede tempo atual
  currentMillis = millis();
 
  //Se ultrapassou o tempo 
  if ((currentMillis - startMillis) >= period && (currentMillis - startMillis) < 30000 && flagtime == 1 )
  {
    ejecao=1;
    startMillis = currentMillis;  //IMPORTANT to save the start time of the current LED state.
    flagtime=3;
  }

  if(flagacel==1){
    if (AcXf < 0 && ejecao == 0) {
      for (int counter_acc = 0; counter_acc < 10; counter_acc++) {
        getAcel (&AcXf, &AcYf, &AcZf, &GyXf, &GyYf, &GyZf);
        mean_acc = mean_acc + AcXf;
        delay(10);
      }
      if (mean_acc < 0) {
        //gerar pulso para a ignição da carga
        ejecao = 1;
        flagacel = 3;
      }
    }
  }
  
  //Se esta pronto para ejectar 
  if (ejecao == 1)
  {
    digitalWrite(sendPin, HIGH);   
    ejecao = -1;
    loraSerial.println("FOI");
  }
}
