package com.example.rocket;

import android.content.Context;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;


public class SerialComms {

    private Comms comms;

    public SerialComms(Context mContext, Comms socket) {

        comms = socket;

        Log.i("serial","Initializing serial");

        comms.sendMessage("Initializing serial");

        UsbManager manager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
//        UsbAccessory accessory = (UsbAccessory) intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
        UsbAccessory[] accessoryList = manager.getAccessoryList();

        try{
            for (UsbAccessory e : accessoryList) {
                //to get key
                comms.sendMessage(e.toString());
            }

            HashMap<String, UsbDevice> deviceList = manager.getDeviceList();

            comms.sendMessage(Boolean.toString(deviceList.isEmpty()));

            for (Map.Entry<String, UsbDevice> e : deviceList.entrySet()) {
                //to get key
                String x = e.getKey();
                //and to get value
                UsbDevice y = e.getValue();
                comms.sendMessage(x + ' ' + y.getDeviceName());
            }

        }catch (NullPointerException e) {

            comms.sendMessage("Erro a fazer coisas");

            comms.sendMessage(e.getMessage());

            e.printStackTrace();
        }





    }

    /*public void read(){
        final int maxCount = 1024;
        byte[] buffer = new byte[maxCount];

        try {
            int count;
            while ((count = mDevice.read(buffer, buffer.length)) > 0) {
                Log.d("serial", "Read " + count + " bytes from peripheral");
                comms.sendMessage("Read " + count + " bytes from peripheral");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        if (mDevice != null) {
            try {
                mDevice.close();
                mDevice = null;
            } catch (IOException e) {
                Log.w("serial", "Unable to close UART device", e);
                comms.sendMessage("Unable to close UART device" + e.getMessage());
            }
        }
    }*/

}
